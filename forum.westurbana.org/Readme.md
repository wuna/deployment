West Urbana Discourse Forum
=========================================================

The WUNA forum is a fairly complex deployment that is simplified by the Discourse `./launcher` script. Launching a Discourse server is basically as easy as cloning the git repo (see subfolder `./discourse`) and constructing a configuration file (see `./discourse/containers/westurbana.yml`). The `example.westurbana.yml` file in this folder is identical to the actual file but with sensitive info redacted.

Data and Backups
-----------------------

Data and backup files are stored under `./discourse/containers/volumes`.

Site settings
-----------------------

Exported using `rake site_settings:export`. See [this discussion for more details](https://meta.discourse.org/t/download-and-restore-of-site-settings/65230/).

```bash
wuna@westurbana:/srv/forum.westurbana.org/discourse$ docker exec -it westurbana rake site_settings:export
```

Current settings (customized, non-default settings only):

```yaml
title: West Urbana Neighborhood Forum
site_description: The West Urbana Neighborhood Forum is a discussion forum for the
  neighborhood in Urbana adjacent to the University of Illinois, notable for its quiet
  charm and the diversity of its many trees.
short_site_description: Forum for residents of the West Urbana neighborhood
contact_email: admin@westurbana.org
contact_url: https://westurbana.org/
notification_email: admin@westurbana.org
site_contact_username: system
company_name: West Urbana Neighborhood Forum
governing_law: Illinois law
city_for_disputes: Urbana, IL
logo: "/uploads/default/original/1X/e33bd19cbc563a8d63a7ee3515fdb1342287ecee.png"
logo_small: "/uploads/default/original/1X/51f751ac0c3b48a9d7c45f7b192ca76e5d6212c7.png"
base_font: lora
heading_font: lora
login_required: 'true'
must_approve_users: 'true'
default_trust_level: '1'
force_https: 'true'
allowed_iframes: https://www.google.com/maps/embed?|https://www.openstreetmap.org/export/embed.html?|https://calendar.google.com/calendar/embed?|https://codepen.io/|http://forum.westurbana.org/discobot/certificate.svg|https://forum.westurbana.org/discobot/certificate.svg
maximum_backups: '7'
backup_frequency: '1'
default_email_digest_frequency: '1440'
tagging_enabled: 'false'
oauth2_enabled: 'true'
oauth2_client_id: '************'
oauth2_client_secret: '************'
oauth2_authorize_url: https://keycloak.reticu.li/auth/realms/WUNA/protocol/openid-connect/auth
oauth2_token_url: https://keycloak.reticu.li/auth/realms/WUNA/protocol/openid-connect/token
oauth2_user_json_url: https://keycloak.reticu.li/auth/realms/WUNA/protocol/openid-connect/userinfo
oauth2_json_user_id_path: sub
oauth2_json_username_path: email
oauth2_json_name_path: name
oauth2_json_email_path: email
oauth2_json_email_verified_path: email_verified
oauth2_json_avatar_path: picture
oauth2_email_verified: 'true'
oauth2_scope: openid
oauth2_button_title: with Single Sign-On
oauth2_allow_association_change: 'true'
```
