WUNA Nextcloud Server
============================================

Containers
------------------

There are three containers in this Docker Compose application:

* Nextcloud
* database
* cron

The Nextcloud container runs the official Nextcloud image, relying on the database container for its MySQL database.

Environment variables are defined in a common `.env` file, containing the relevant database credentials and other script variables. Copy the `sample.env` to `.env` and customize the values.

[Nextcloud requires a periodic execution of a script that performs background tasks, like sending notification emails about activity](https://docs.nextcloud.com/server/21/admin_manual/configuration_server/background_jobs_configuration.html). This is accomplished by a third `cron` container, which executes the script every ten minutes.

Volumes
------------------

The user data files and Nextcloud files are stored in `./volumes/www`. The Nextcloud database is stored in `./volumes/db`.
