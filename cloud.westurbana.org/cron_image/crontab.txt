# crontab file
*/10 * * * * export PHP_MEMORY_LIMIT=64M && /usr/local/bin/php -f /var/www/html/cron.php >> /var/www/html/cron.log
#*/1 * * * * echo "The time is $(date)" >> /tmp/cron.log
# This extra line makes it a valid cron