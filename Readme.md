West Urbana Neighborhood Association (WUNA) Web Services
=========================================================

This is the top-level directory containing WUNA web services deployed as [Docker Compose applications](https://docs.docker.com/compose/). Read [this useful tutorial for how to create a WordPress app](https://docs.docker.com/compose/wordpress/) to learn more about how Docker Compose works.

List of WUNA web services:

- [WordPress website](./wuna/Readme.md)
- [Nextcloud server](./cloud.westurbana.org/Readme.md)
- [Discourse forum](./forum.westurbana.org/Readme.md)
- [HedgeDoc server](./docs.westurbana.org/Readme.md)

These services are independent in terms of their deployment and are self-contained in their subfolders. However, all of them require that [Traefik](./traefik) is running. Traefik is the router that listens on ports 80/443 and acts as a reverse proxy to route web traffic to the targeted service based on the web request. Traefik also automatically obtains and renews TLS certificates to enable `https` secure connections by default.
