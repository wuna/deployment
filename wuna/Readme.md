WUNA WordPress Website
============================================

Containers
------------------

There are three containers in this Docker Compose application:

* WordPress
* database
* backup utility

The WordPress container runs the official WordPress image, relying on the database container for its MySQL database.

Environment variables are defined in a common `.env` file, containing the relevant database credentials and other script variables. Copy the `sample.env` to `.env` and customize the values.

Volumes
------------------

The WordPress files are stored in `./volumes/www`. The WordPress database is stored in `./volumes/db`. Backups are stored in `./volumes/backups`.

Backups
------------------

The backup utility is a container running `cron` that will take snapshots of the database via `mysqldump` and also the WordPress files, storing them as compressed `tar` files. Backups occur once per week, and backup files older than a month are deleted.
