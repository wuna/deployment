#!/bin/bash

# Delete all backup files older than $num_days
num_days=$1
for days in $(seq $num_days 365); do 
    backup_file="/backups/wordpress."$(date --date="-$days days" +%Y%m%d).tgz
    # echo "Scanning backup: ${backup_file}" 
    if [ -f "${backup_file}" ]; then
        echo "Deleting backup: ${backup_file}" 
        rm "/backups/wordpress."$(date --date="-$days days" +%Y%m%d).tgz;
    fi
done