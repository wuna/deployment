#!/bin/bash -e

# Dump the database to a sql file and take a snapshot of the web files. 
# Compress all files in a gzip tar file.

BASE_PATH="/backups"
WEB_FILES="/wordpress"
BASE_NAME="wordpress.$(date +%Y%m%d)"
SQL_FILE="${BASE_PATH}/${BASE_NAME}.sql"
BACKUP_FILE="${BASE_PATH}/${BASE_NAME}.tgz"

mysqldump \
--host=db \
--user=${WORDPRESS_DB_USER} \
--password=${WORDPRESS_DB_PASSWORD} \
${WORDPRESS_DB_NAME} \
2>&1 | grep -v "mysqldump:" > "${SQL_FILE}"

tar -czf "${BACKUP_FILE}" "${SQL_FILE}" "${WEB_FILES}"
chown $BACKUP_UID:$BACKUP_GID "${BACKUP_FILE}"
rm "${SQL_FILE}"