Collaborative document editing with HedgeDoc
===============================================

[HedgeDoc](https://hedgedoc.org/) (formerly known as CodiMD) is an open-source, web-based, self-hosted, collaborative markdown editor.

You can use it to easily collaborate on notes, graphs and even presentations in real-time. All you need to do is to share your note-link to your co-workers and they’re ready to go.

## Configuration

The configuration is described in the [online docs](https://docs.hedgedoc.org/configuration/). The secrets and other config parameters are set via environment variables in `.env`. The `env.template` provides a template for the configuration currently in use. 

## User Accounts

The service is configured to use OpenID Connect to provide authentication via a Keycloak server, allowing people to log in using existing identity providers like Google or GitHub. They must be added to the relevant Keycloak user group in order to be authorized to create notes.

User accounts based on email addresses can be managed using the `manage_users` utility. Log into the WUNA host machine and use `docker exec` to execute the commands in the HedgeDoc container:

```bash
# Get a terminal in the container
wuna@westurbana:/srv$ docker exec -it hedgedoc-app bash

# Show utility help info
hackmd@978b051a654c:~/app$ bin/manage_users --help
You did not specify either --add or --del or --reset!

Command-line utility to create users for email-signin.
Usage: bin/manage_users [--pass password] (--add | --del) user-email
  Options:
    --add	Add user with the specified user-email
    --del	Delete user with specified user-email
    --reset	Reset user password with specified user-email
    --pass	Use password from cmdline rather than prompting

# Add a new user bob@example.com
hackmd@978b051a654c:~/app$ bin/manage_users --pass P@55w0Rd --add bob@example.com 

```
